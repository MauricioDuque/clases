alert("Hola mundo")
alert("Soy el primer script")

alert("Este es el ejercicio 2")

var texto1 = "Así de facil es usar \'comillas simples\' y \"comillas dobles\" "
alert(texto1)

alert("este es el ejercicio 3")
var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
alert (meses[0])
alert (meses[1])
alert (meses[2])
alert (meses[3])
alert (meses[4])
alert (meses[5])
alert (meses[6])
alert (meses[7])
alert (meses[8])
alert (meses[9])
alert (meses[10])
alert (meses[11])

alert("Este es el ejercicio 4")
var valores = [true, 5, false, "hola", "adios", 2]
alert("Ejercicio 4.1")

var mayor = valores[4] > valores[3]

alert(mayor)



alert("ejercicio 4.2")

var or = valores[0] || valores[2]

alert(` "${or}" se utilizo una funcion or`)

var and = valores[0] && valores[2]

alert(` "${and}" se utilizo una funcion and`)
alert("ejercicio 4.3")

var suma = valores[1] + valores[5]
var resta = valores[1] - valores[5]
var mul = valores[1] * valores[5]
var div = valores[1] / valores[5]
var resto = valores[1] % valores[5]

alert(`${valores[1]} + ${valores[5]} = ${suma}`)
alert(`${valores[1]} - ${valores[5]} = ${resta}`)
alert(`${valores[1]} * ${valores[5]} = ${mul}`)
alert(`${valores[1]} / ${valores[5]} = ${div}`)
alert(`el resto entre ${valores[1]} y ${valores[5]} es ${div}`)

alert("Ejercicio 5")
var numero1 = 5
var numero2 = 8
var numero3 = numero1 * (-1)
var numero4 = numero1++

if(numero1 > numero2){
    alert(` ${numero1} es mayor que ${numero2}`)
}else {
    alert(` ${numero2} es mayor que ${numero1}`)
}

if(numero2 >= 0){
    alert(` ${numero2} es positivo`)
}else{
    alert(` ${numero2} es negativo`)
}

if(numero3 >= 0){
    alert(` ${numero1} es positivo o igual a 0`)
}else{
    alert(` -${numero1} es negativo o distinto a 0`)
}

if(numero4 > numero2){
    alert(` aumentar en 1 unidad a ${numero1} lo hace mayor que ${numero2}`)
}else{
    alert(` aumentar en 1 unidad a ${numero1} no lo hace mayor o igual que ${numero2}`)
}

alert("Ejercicio 6")
var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var DNIT = 23

var numDNI = prompt("Ingresa tu DNI")
var letDNI = prompt("Ingresa la letra de tu DNI en mayuscula")


if (numDNI < 0 || numDNI > 99999999){
    alert("El numero que ingresaste es incorrecto")
}else{
    var restoDNI = numDNI % DNIT
    if(letDNI == letras[restoDNI]){
        alert("Los datos coinciden")
    }else{
        alert("Los datos ingresados son icorrectos")
    }
}

alert("Ejercicio 7")

var numero5 = prompt("Ingresa un numero")
var fact = 1
var i
if(numero5 < 0){
    fact = 0
}else if (numero5 == 0){
    fact = 1
}else {
    for (i = 1; i<= numero5; i++){
        fact = fact*i
    }
}
alert(`El resultado es ${fact}`)

alert("Ejercicio 8")

var numero6 = prompt("Ingresa un numero entero")
var res = parImpar(numero6)

alert(`El numero ${numero6} es ${res}`)

function parImpar(numero6){
    if(numero6 % 2 == 0){
        return "Par"
    }else{
        return "Impar"
    }
}

alert("Ejercicio 9")

var cadena = prompt("Ingrese un texto")
var inf = info(cadena)

function info(cadena){
    var resultado1 = (`La cadena ${cadena}`)
 
    if(cadena == cadena.toUpperCase()) {
      alert (` ${resultado1}  está formada sólo por mayúsculas`)
    }
    else if(cadena == cadena.toLowerCase()) {
      alert (`${resultado1}  está formada sólo por minúsculas`)
    }
    else {
      alert ( `${resultado1}  está formada por mayúsculas y minúsculas`)
    }
   
    return resultado1
  }
   
  

