Vue.component('product-details', {
    props: {
      details: {
        type: Array,
        required: true
      }
    },
    template: `
      <ul>
        <li v-for="detail in details">{{ detail }}</li>
      </ul>
    `
  })

Vue.component ( 'product', {
    props: {
        premium: {
            type: Boolean,
            required: true
        }
    },
    template:`
    <div class="product">

            <div class="product-image">
                <img v-bind:src="image" width="150">
            </div>

            <div class="product-info">
                <h1>{{ title }}</h1>
                <p v-if="inStock">In stock</p>
                <p v-else :class="{outStock: !inStock}">Out of stock</p>
                <p>Shipping: {{  shipping  }}</p>

                <product-details :details="details"></product-details>

                <div v-for="(variant, index) in variants" 
                    :key="variant.variantId"
                    class = "color-box"
                    :style="{ backgroundColor: variant.variantColor}"
                    @mouseover="updateProduct(index)">
                </div>
            
                <button v-on:click="addToCart" 
                style="color: azure;" 
                :disabled="!inStock"
                :class="{ disabledButton: !inStock}"
                >Add to cart</button>
                  
                <button v-on:click="removeToCart" 
                style="color: beige;" 
                :disabled="!inStock"
                :class="{ disabledButton: !inStock}"
                >Remove to cart</button>
              
            </div>

            <div id="reviews">
                <p v-if="!reviews.length">There are no reviews yet.</p>
                <ul v-else>
                    <li v-for="(review, index) in reviews" :key="index">
                    <p>{{ review.name }}</p>
                    <p>Rating:{{ review.rating }}</p>
                    <p>{{ review.review }}</p>
                    </li>
                </ul>
            </div>
            <product-review @review-submitted="addReview"></product-review>
        </div>

    `, data (){
        return {
        product: 'Socks',
            brand: 'Vue Mastery',
            selectedVariant: 0,
            details: ["80% cotton", "20% polyester", "Gender-neutral"],
            variants: [
                {
                    variantId: 2234,
                    variantColor: "red",
                    variantImage: 'https://statics.glamit.com.ar/media/catalog/product/cache/74/image/800x1200/9df78eab33525d08d6e5fb8d27136e95/9/0/909_carocuore_6475d5_d5_1.jpg',
                    variantQuantity: 10
                },
                {
                    variantId: 2235,
                    variantColor: "grey",
                    variantImage: 'https://arturocalle.vteximg.com.br/arquivos/ids/205517-1200-1598/HOMBRE-MEDIAS-10069400-GRIS_1.jpg?v=636981257408370000',
                    variantQuantity: 0
                }
            ],
            sizes: ["S", "M", "L", "XL", "XXL"],
            reviews: []
            }
        },   // fin data
        
        methods: {
            addToCart () {
                this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
            },

            updateProduct(index) {
                this.selectedVariant = index
                console.log(index)
            },

            removeToCart () {
                this.$emit('remove-to-cart', this.variants[this.selectedVariant].variantId)
            },

            addReview (productReview){
                this.reviews.push(productReview)
            }
        },

        computed: {
            title (){
                return this.brand + ' ' + this.product
            },
            image (){
                return this.variants[this.selectedVariant].variantImage
            },
            inStock (){
                return this.variants[this.selectedVariant].variantQuantity
            },
            shipping (){
                if (this.premium){
                    return "Free"
                } else {
                    return 2.99
                }
            }
        }

})

Vue.component('product-review',{
    template:`

    <form class="review-form" @submit.prevent="onSubmit">

        <p class="error" v-if="errors.length">
            <b>Please correct the following error(s):</b>
            <ul>
            <li v-for="error in errors">{{ error }}</li>
            </ul>
        </p>

        <p>
            <label for="name">Name:</label>
            <input id="name" v-model="name" placeholder="name">
        </p>
        
        <p>
            <label for="review">Review:</label>      
            <textarea id="review" v-model="review"></textarea>
        </p>
        
        <p>
            <label for="rating">Rating:</label>
            <select id="rating" v-model.number="rating">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
            </select>
        </p>
            
        <p>Would you recommend this product?</p>

        <div id="yesNo">
            <label>
                Yes
                <input type="radio" style="alignLeft" value="Yes" v-model="recommend"/>
            </label>
            
            <label>
                No
                <input type="radio" value="No" v-model="recommend"/>
            </label>
        </div>

        <div id="submit">
            <p>
                <input type="submit" value="Submit">  
            </p>    
        </div>

    </form>
    
    `,
    data() {
        return {
            name: null,
            review: null,
            rating: null,
            recommend: null,
            errors: []
        }
    },
    methods: {
        onSubmit() {
          this.errors = []
          if(this.name && this.review && this.rating && this.recommend) {
                let productReview = {
                    name: this.name,
                    review: this.review,
                    rating: this.rating,
                    recommend: this.recommend
                }
                this.$emit('review-submitted', productReview)
                this.name = null
                this.review = null
                this.rating = null
                this.recommend = null
            } else {
                if(!this.name) this.errors.push("Name required.")
                if(!this.review) this.errors.push("Review required.")
                if(!this.rating) this.errors.push("Rating required.")
                if(!this.recommend) this.errors.push("Recommendation required.")
            }   
        }       
     }
    
})


var app = new Vue({
    el: '#app',
    data: {    
        premium: false,
        cart: []
    },  

    methods: {
        updateCart (id){
            this.cart.push(id)
            
        },
        remCart (id){  
            for(var i = this.cart.length - 1; i >= 0; i--) {
                if (this.cart[i] === id) {
                this.cart.splice(i, 1);
                }
            }
        }
    }
})