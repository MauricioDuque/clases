// Ejercicios

let nombres = ['juan', 'pedro', 'oscar', 'camilo']

// 1 determinar el tamaño del arreglo 
// 2 Invertir el arreglo ['camilo', 'oscar'...]
// 3 Dividir el arreglo -> una funcions que divida el arreglo
// |_ ['juan', 'pedro'] ['oscar', 'camilo']


// Ejercicio 1 
let nombres = ['juan', 'pedro', 'oscar', 'camilo']
// 1.1 determinar el tamaño del arreglo 
    // 1.1.1 Solución a
let cantidad = 0
for (const key in nombres) {
    cantidad = key
}
console.log(++cantidad)
console.log(nombres[1])
    // 1.1.2 Solución b
console.log(nombres.length)
    // 1.1.3 solucion c
let total
for (i=0; i<nombres.length; i++) {
    total = i
}
console.log(++total)

// 1.2 Invertir el arreglo ['camilo', 'oscar'...]
const nombres = ['juan', 'pedro', 'oscar', 'camilo']
console.log('nombres:', nombres);
const invertido = nombres.reverse();
console.log('invertido:', invertido);


// 1.3 Dividir el arreglo
    //1.3.1 Solución a
nombres.length = 2
console.log(nombres)
    //1.3.2 Solución b
let nombres = ['juan', 'pedro', 'oscar', 'camilo', 'ramiro']
let m1 = nombres.splice(0,(nombres.length/2));
console.log("Mitad 1 --> ",m1);
let m2 = nombres.splice(0,nombres.length);
console.log("Mitad 2 -->",m2);
    //1.3.3 Solución c
let nombres = ['juan', 'pedro', 'oscar', 'camilo', 'ramiro']
let mitad = Math.floor(nombres.length / 2)
let inicio = nombres.slice(0, mitad)
let final = nombres.slice(mitad)
console.log([inicio.toString()])
console.log([final.toString()])
    //1.3.4 Solución d
var nombres = ['juan', 'pedro', 'oscar', 'camilo', 'ramiro']
var index = nombres.length
var primeraParte = nombres.slice(0, index/2)
var segundaParte = nombres.slice(index/2,index)
console.log(primeraParte)
console.log(segundaParte)
    //1.3.5 Solución e
var nombres = ['juan', 'pedro', 'oscar', 'camilo', 'ramiro', 'jose']
var index = nombres.length
var primeraParte = nombres.slice(0, index/3)
var segundaParte = nombres.slice(index/2,index)
var terceraParte = nombres.slice(index/3,index)
console.log(primeraParte)
console.log(segundaParte)
console.log(terceraParte)
    //1.3.6 Solución f
let nombres = ['juan', 'pedro', 'oscar', 'camilo', 'ramiro', 'jose']
partes = []; // Aquí almacenamos los nuevos arreglos
const divisiones = 1; // Partir en arreglo en 2 cada uno
for (let i = 0; i < nombres.length; i += divisiones) {
    let pedazo = nombres.slice(i, i + divisiones);
    partes.push(pedazo);
}
console.log(partes)

//Ejercicio 2
    //Método de la burbuja
    let numeros = [2, 1, 5, 7, 3, 6, 0, 9, 8, 4];

    // function Burbuja() {
var numeros = [2, 1, 5, 7, 3, 6, 0, 9, 8, 4];
var n, i, k, aux;
n = numeros.length;
console.log(numeros); 
// Algoritmo de burbuja
    //ascendete
for (k = 1; k < n; k++) {
    for (i = 0; i < (n - k); i++) {
        if (numeros[i] > numeros[i + 1]) {
            aux = numeros[i];
            numeros[i] = numeros[i + 1];
            numeros[i + 1] = aux;
        }
    }
}
    //descendete
console.log(numeros); 
const invertido = numeros.reverse();
console.log('invertido:', invertido);


// Sin contar el cero, R = 362880
var total = 1;
let numeros = [2, 1, 5, 7, 3, 6, 9, 8, 4];
function producto(){
  for (var f=0;f<numeros.length;f++){
    if(numeros[f]!=0){
      total = (numeros[f]*total);
    }else total = 0;
  }return total;
}
console.log(producto());

// Contando el 0, la respuesta sería 0
var total = 1;
let numeros = [2, 1, 5, 7, 3, 0, 6, 9, 8, 4];
function producto(){
  for (var f=0;f<numeros.length;f++){
    if(numeros[f]!=0){
      total = (numeros[f]*total);
    }else total = 0;
  }return total;
}
console.log(producto());

// Conocer posición de un elemento en un arreglo
// var numeros = ['a', 'b', 'a', 'c', 'a', 'd'];
var indices = [];
let numeros = [2, 1, 5, 7, 3, 0, 6, 9, 8, 4];

var element = 1;
var idx = numeros.indexOf(element);
while (idx != -1) {
  indices.push(idx);
  idx = numeros.indexOf(element, idx + 1);
}
console.log(indices);
// 
    // Solución B 
let numeros = [2, 1, 5, 7, 3, 0, 6, 9, 8, 4];
let elemento = 5;
let  elementoReal = numeros.indexOf(elemento);
console.log(`La posición de  ${elemento} es:`, elementoReal + 1); 

// Ejercicio 3
let mixin = ['juan', 2, 'pedro', false, 15, 34.15, ,]
let tipo = mixin[6];

if (typeof tipo == 'string'){
    console.log('Es de tipo string');
}else if (typeof tipo == 'boolean'){
    console.log('Es de tipo booleano');
}else if (typeof tipo == 'float'){
    console.log('Es de tipo flotante');
}else if (typeof tipo == 'undefined'){
    console.log('No está definido');
}else {
    console.log('Es un numero entero');
}

// Nuevo spread operator
    // metodo normal de concatenación
    let arr = [1,2,3]; 
    let arr2 = [4,5];  
    arr = arr.concat(arr2); 
    console.log(arr);
    //Ahora si jaja 
    let arr = [1,2,3]; 
    let arr2 = [4,5]; 
    arr = [...arr,...arr2]; 
    console.log(arr);
    // Método para expandir
    let arr = ['a','b']; 
    let arr2 = [...arr,'c','d']; 
    console.log(arr2); // [ 'a', 'b', 'c', 'd' ]