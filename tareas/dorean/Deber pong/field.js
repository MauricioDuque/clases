  var controlCountX = true
  var controlCountY = true
  var counterX = 0
  var counterY = 0
  var playerA = 0
  var playerB = 0

class Field {
  constructor(canvas){
    this.width = canvas.width 
    this.height = canvas.height 
    this.ctx = canvas.getContext('2d') 
  }

  init() {
    this.ball = new Ball()
  }

  exe(){
    this.clear()
    this.drawCmp()
    this.controlCounter()
    this.square(counterX + 100, counterY - 10)
    this.world(counterX + 100, counterY - 10)
  }

  clear (){
    this.ctx.clearRect(0,0, this.width, this.height)
  }

  drawCmp(){
    this.ctx.fillRect(
      this.ball.getPosition().x,
      this.ball.getPosition().y,
      this.ball.size,
      this.ball.size
    )
  }

  square (x, y){
    this.x = x
    this.y = y
    this.ctx.fillRect(this.x, this.y, this.ball.size, this.ball.size)
  }

  controlCounter () {
    console.log(`PlayerA = ${playerA}  PlayerB = ${playerB}`)
    if(controlCountX) counterX++
    else counterX--
    if(controlCountY) counterY++
    else counterY--
  }

  world (x, y){
    if(x + this.ball.size >= this.width) {
      controlCountX = false
      if(controlCountX == false) playerA++
    }
    if(y + this.ball.size >= this.height) controlCountY = false
    if(x <= 0) {
      controlCountX = true
      if(controlCountX == true) playerB++
    }
    if(y <= 0) controlCountY = true
  }
}
