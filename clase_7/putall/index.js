var canvas = null
var ctx = null
var counter = 0

window.onload = () => {
  canvas = document.getElementById("myCanvas")
  ctx = canvas.getContext("2d")
}

let exec = () => {
  counter++
  // ctx.clearRect(0,0, 600, 600)
  ctx.fillRect(counter, counter, 150, 100)
  if(counter >= 600 - 100) counter = 0
}

setInterval(() => {
  exec()
}, 10);