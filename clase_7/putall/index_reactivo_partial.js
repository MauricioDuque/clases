var _titulo = 'Este es el titulo'
var parrafo = 'Hello'

const init = () => {
  const element = document.createElement('p')
  const titulo = document.createElement('h1')
  const container = document.getElementById('app')
  
  element.innerText = parrafo
  titulo.innerText = _titulo
  
  container.appendChild(titulo)
  container.appendChild(element)
}

const exec = (valor) => {
  _titulo = valor
  init()
}


window.onload = function () {
  init()
}

let contador = 0
setInterval(() => {
  exec(`el contador va en ${contador}`)
  contador++
}, 3000);

