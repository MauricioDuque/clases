// Esta es la explicacion de scope de las variables
// Diferencia entre 'var' y 'let'

{
	var globalVar = 'Esto es una varible global'
}

{
	let globalLet = 'Esto es una variable *no es global, solo se puede ejecutar en dentro de su espacio, es decir solo entre los corchetes'
	console.log(globalLet)
}

console.log(globalVar)
console.log(globalLet)

// funcionalidad spread

let team1 = ['David', 'Dorean']
let team2 = ['Mauro', 'Pipe']

let teamAll = ['Juan', ...team1, ...team2]
// console.log(['Juan', team1, team2])
// ['Juan','David', 'Dorean', 'Mauro', 'Pipe']

console.log(teamAll)


// Symbols
let mySymbl = `Mi simbolo`
let symbol = Symbol(mySymbl)
console.log(symbol.description)

// Entries
let entries = [['name', 'oscar'], ['age', 22]]
console.log(Object.fromEntries(entries))

// {
// 	name: 'Oscar',
// 	age: 22
// }

// trims
let hello = 'hello world         '
console.log(hello)
console.log(hello.trimEnd())

let hello2 = '        hello 2'
console.log(hello2)
console.log(hello2.trimStart())

//Matrices
let array = [1,2,3, [1,2,3, [1,2,3]]]
console.log(array.flat(2))

let array = [1,2,3,4,5]
console.log(array.flatMap(value => [value, value*2]))

// Regex
const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/
const match = regexData.exec('2018-04-20')
const year = match[1]
const month = match[2]
const day = match[3]
console.log(year, month, day)



