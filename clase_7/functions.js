// Esta clase son los nuevos estandares de las funciones, tambien de los objetos y las clases
//

// Herencias
const obj = {
	name: 'Mauricio',
	age: 32,
}

const obj1 = {
	...obj,
	country: 'CO'
}

console.log(obj1)

// funcionalidad nueva de es6
const obj = {
	name: 'Mauricio',
	age: 32,
}

// const {name, age} = obj
// // name = obj.name
// // age = obj.age
// console.log(name, age)

// function(name, age, country){
// 	this.name = name
// 	...
// }

// spread en objetos
//

let {...all} = obj1
console.log(all)

let {country, ...all} = newObj
console.log(all)

let {name, ...all} = newObj2
console.log(name, all)

// Mixin de funciones
const string = 'Hello'
console.log(string.padStart(7, 'hi'))
console.log(string.padEnd(12, ' -----'))
console.log('Food'.padEnd(12, ' ----'))

// funciones en objetos
const data = {
	frontend: 'Mauricio',
	backend: 'Dorean',
	design: 'David',
}
const values = Object.values(data)
console.log(values)

const entries = Object.entries(data)
console.log(entries)

// Funciones expoencial

let base = 4
let exponent = 3
let result = base ** exponent
console.log(result)

// Funcione en array
let numbers = [1,2,3,4,7,8]
if(numbers.includes(9)) console.log('encuentra el valor 7')
else console.log('no encuentra nada')

// Funciones especiales para hacer pasos
//
let count = 0
function helloWorld(){
	if(count==0) {
		...
		count = 1
	}
	if(count == 1) {
		...
		count = 0
	}
}

helloWorld()
helloWorld()


function* helloWorld() {
	if(true) yield 'hello, '
	if(true) yield 'World '
}

const generatorHello = helloWorld()
console.log(generatorHello.next().value)
console.log(generatorHello.next().value)
console.log(generatorHello.next().value)

// Clases
//
// function calculator () {

// }

// calculator.prototype

class calculator {
	constructor() {
		this.valueA = 0
		this.vlaueB = 0
	}
	sum(valueA, valueB) {
		this.valueA = valueA
		this.valueB = valueB
		return this.valueA + this.valueB
	}
}

let cal = new calculator()
console.log(cal)

// Function arrows
//  es5
// function nameFunc (name) {
// 	....
// 	return name*name
// }
// let listOfNames = nameFunc()

// es6
const listOfNames = (name, age, country) => {...}
const listOfNames = name => {...}
const square = num => num * num

// Map
let names = ['juan', 'pedro', 'jasinto', 'dorean']

// for (let index = 0; index < names.length; index++) {
// 	const element = names[index];
// 	console.log(element)
// }

let listOfNames = names.map(item => {
	console.log(item)
})



