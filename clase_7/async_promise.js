// Esta clase contiene aquellas funciones asincronas, son aquellas 
// que no tienen una respuesta inmediata, como la consulta a una base 
// de datos, la cual puede tardar en responder por razones como el tamano 
// o cantidades de procesos que tiene que hacer, incluso por que hace una 
// consulta a un servidor externo, por esta razon es necesario funciones que 
// una vez termiada la accion puede responder al requerimiento.

// Promesas
const helloPromise = () => {
	return new Promise((resolve, reject) => {
		if(true) setTimeout(()=> resolve ('hi'), 3000)
		else reject ('Ups!')
	})
}

console.log(helloPromise())

const helloPromise = () => {
	setTimeout(() => {return 'hola'}, 3000)
}
console.log(helloPromise())

// Async & Await
const helloWorld = () => {
	return new Promise((resolve, reject) => {
		(false)
			? setTimeout(() => resolve('hello'), 3000)
			: reject(new Error('test'))
	})
}

helloWorld()
	.then(response => console.log(response))
	.catch(error => console.log(error))
	.finally(() => console.log('Finalizo'))


const helloAsync = async () => {
	const hello = await helloWorld()
	console.log(hello)
}

const helloWorld = () => {
	return new Promise((resolve, reject) => {
		(true)
			? setTimeout(() => resolve('hello'), 3000)
			: reject(new Error('test'))
	})
}

const anotherFunction = async () => {
	try {
		const hello = await helloWorld()
		console.log(hello)
	} catch {
		console.log(error)
	}
}

console.log(anotherFunction())
