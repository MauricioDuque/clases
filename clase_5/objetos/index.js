window.onload = function() {
}

// Reglas de programacion
// -- todos los archivos empizan con minuscolas y camelCase i.e: mauricioDuque.html
// -- todos los archivos primarios son index
// -- todas la variables son camelCase 
// -- Todas las clases empiezan con mayuscula
// -- Metodos en camelCase

//  Objetos

// Clase
function Felinos (nombre, patas, colas, boca, color, sexo) {
  this.nombre = nombre || 'No tiene nombre'
  this.patas = patas
  this.colas = colas
  this.boca = boca
  this.color = color
  this.sexo = sexo
}

// Constructor
let rose = new Felinos('Rose', 4, 1, 1, 'Cafe', 'Hembra')
let jerry = new Felinos('Jerry', 4, 1, 1, 'Gris', 'Macho')
let nose = new Felinos(null, 4, 1, 1, 'Gris', 'Macho')

// Metodos

Felinos.prototype.cazar = function () {
  console.log(`${this.nombre} esta cazando un raton`)
}

// Herencia

function ClasesFelinos (felino, tipo) {
  felino.call(this, felino)
  this.tipo = tipo
}

// ClasesFelinos.prototype = Object.create(rose, 'gato')