window.onload = function() {
}

// Reglas de programacion
// -- todos los archivos empizan con minuscolas y camelCase i.e: mauricioDuque.html
// -- todos los archivos primarios son index
// -- todas la variables son camelCase 

//  Tipos de variables

// boolean (true/false) 1 o 0
//  Var es la version antigua
var on = true 
var off = false

// let es la version nueva de var
let _on =  true
let _off = false

// const no puedo renombrar la variable
const _On = true
const _Off = false

let tengoPlata = new Boolean
tengoPlata = true

// Numerica
let numero = 1 // Entero
let num = 1.25 // Flotante

// String
let nombre = 'mauricio'

// Arrays
//  clousters, arreglos, vectores, matriz
// [1, 2, 3, 4, 5] => numerico
// ['a', 'b'. 'c'] => String
// ['a', 1, 'b', 2] => mixto

// Denotar
let frutas = []
let fruits = new Array

frutas = ['banano', 'sandia', 'naranja']
fruits = ['Apple', 20]
