window.onload = function() {
}

// Reglas de programacion
// -- todos los archivos empizan con minuscolas y camelCase i.e: mauricioDuque.html
// -- todos los archivos primarios son index
// -- todas la variables son camelCase 

// Estructuras if

if(true){} // true

let num = 0
if(num == 1) {}
else if (num == 0) {}
else {}

// for
let frutas = ['peras', 'naranjas', 'platanos']
for (let index = 0; index <= frutas.length; index++) {
  console.log(index)
}

// console.log(index)

for (const key in frutas) {
  console.log(frutas[key])
}

// console.log(frutas[key])

while(num <= 10){
  console.log(num)
  num++
}

do {
  num++
  console.log(num)
}while(num <= 10)


let variable = 'd'
switch(variable) {
  case 'a':
    console.log('la letra es A')
    break
  case 'b':
    console.log('la letra es B')
    break
  case 'c':
    console.log('la letra es C')
    break
  case 'd':
    console.log('la letra es D')
    break
  default:
    console.log('la letra es E')
}

// Scope
// Locales - Globales

// funciones - arrays
let nombre = 'mauricio'
nombre.indexOf('a') // 1
nombre.indexOf('i') // 4
nombre.lastIndexOf('i') // 6
nombre.substring(2)
nombre.substring(2, 6)
nombre.length // 8
nombre.split("") // ["m", "a", "u", "r", "i", "c", "i", "o"]
nombre.split("").join("") // "mauricio"

frutas.pop() // "platanos"
frutas.shift() // "peras"
frutas.push("uvas") // ["peras", "naranjas", "platanos", "uvas"]
frutas.unshift("0") // ["0", "naranjas", "platanos", "uvas"]
frutas.reverse() 

//  concatenados
let num1 = [1, 2, 3]
let num2 = [4, 5, 6]

num1.concat(num2) // [1, 2, 3, 4, 5, 6]

num2.concat(num1) // [4, 5, 6, 1, 2, 3]

let numFlot = 1.234
console.log(numFlot.toFixed(1)) // 1.2
