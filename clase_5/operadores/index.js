window.onload = function() {
}

// Reglas de programacion
// -- todos los archivos empizan con minuscolas y camelCase i.e: mauricioDuque.html
// -- todos los archivos primarios son index
// -- todas la variables son camelCase 

// Asignacion
let nombre = 'dorean'

// incremento
let num = 5
++num // incrementa y despues actualiza
num-- // actualiza y despues decrementa
num += 3 // 8
num -= 1 // 4
num *= 2 // 10
num /= 5 // 1
num %= 4 // 1

// Logicas
// [!] negacion
// [&&] and
// [||] or
// [>] mayor que
// [<] menor que
// [==] igual que
// [!=] diferente
let numA = 0
let numB = 0
let numC = 1

numA == numB || numA == numC


// Negacion
let on = true
!on



