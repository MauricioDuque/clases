let mauro = new MaosCat('mauro')
mauro.name = 'mauro'
mauro.sexo = 'macho'
mauro.type = 'Leon'
mauro.color = '#fffff'

let david = new MaosCat('david')
david.name = 'david'
david.sexo = 'hembra'
david.type = 'Gata'
david.color = '#33333'

let gatosEnPelea = [mauro, david]
let ataques = 4

let ataqueMauro = 0
let ataqueDavid = 1
for (let index = 0; index < ataques; index++) {
  if(ataqueMauro == 0) {
    mauro.garraDoble(david)
    ataqueMauro = 1
    ataqueDavid = 0
  }
  if(ataqueDavid == 0) {
    david.garraSimple(mauro)
    ataqueDavid = 1
    ataqueMauro = 0
  }
}
