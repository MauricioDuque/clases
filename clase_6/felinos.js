// Clase
function Felinos () {
  this.patas = 4
  this.color = null
  this.sexo = null
  this.vida = true
  this.estado = 10
  this.type = null
}

// Metodos

Felinos.prototype.cazar = function () {
  console.log('Accion cazar')
}

Felinos.prototype.huir = function () {
  console.log('Accion huir')
}

Felinos.prototype.correr = function () {
  console.log('Accion correr')
}

Felinos.prototype.atacar = function () {
  console.log('Accion atacar')
}

Felinos.prototype.resetEstado = function() {
  this.estado = 10
}

Felinos.prototype.setAtaque = function(num) {
  this.estado -= num
  if(this.estado <= 0) {
    this.vida = false
    this.estado = 0
  }
} 