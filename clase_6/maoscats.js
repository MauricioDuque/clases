function MaosCat (nombre) {
  Felinos.call(this, nombre)
}
MaosCat.prototype = Object.create(Felinos.prototype)
MaosCat.prototype.constructor = MaosCat

MaosCat.prototype.setLive = function () {
  this.vida = true
  this.estado = 10
}

MaosCat.prototype.garraDoble = function(felino) {
  felino.setAtaque(3)
}

MaosCat.prototype.garraSimple = function(felino) {
  felino.setAtaque(1)
}
