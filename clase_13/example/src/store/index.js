import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import {auth} from '@/store/firebaseConfig';

export default new Vuex.Store({
  state: {
    user: null,
  },
  mutations: {
  },
  actions: {
    CREAR_USUARIO: ({state}, payload) => {
      
      auth.createUserWithEmailAndPassword(payload.email, payload.password)
        // .then(data => state.user = data)
        .catch(err => alert(`Error al crear el usuario: ${err}`))
    },
    SIGN_OUT_USER: (context, payload = {}) => {
      auth.signOut()
        .then(() => {
          alert('Exito SignOut')
        }).catch((error) => {
          alert(`Error ${error}`)
        });
    }
  },
  modules: {
  }
})
