import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'


if (!firebase.apps.length) firebase.initializeApp({

})

const db = firebase.firestore()

const auth = firebase.auth()


export { db, auth }



