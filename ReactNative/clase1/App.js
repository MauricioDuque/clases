import React, {Component} from 'react';
import {Alert, StyleSheet, Image, View, TouchableOpacity} from 'react-native';

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          Alert.alert(
            'Alerta',
            'Desea cambiar el logo?',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
        }}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'https://reactnative.dev/img/tiny_logo.png',
          }}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 150,
    height: 150,
    marginLeft: 10,
    borderRadius: 150 / 2,
    borderColor: 'red',
    borderWidth: 3,
  },
});
