import React, {Component} from 'react';
import Vistacmp from '../components/vista';

class Vista extends Component {
  constructor() {
    super();
  }
  presiona() {
    console.log("Presiona nombre");
  }
  render() {
    return <Vistacmp onPress={() => this.presiona()} name={this.props.nombre} />;
  }
}

export default Vista;