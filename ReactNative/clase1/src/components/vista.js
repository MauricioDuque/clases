import React, {useState} from 'react';
import {Text, StyleSheet, TouchableOpacity, View} from 'react-native';

let Vista = () => {
  const [count, setCount] = useState(0);
  const onPress = () => setCount((prevCount) => prevCount + 1);
  return (
    <View style={styles.container}>
      <View style={styles.countContainer}>
        <Text>Count: {count}</Text>
      </View>
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Text>Press Here</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    marginTop: 10,
    width: 200,
    height: 25,
    borderRadius: 6,
    alignSelf: 'flex-end',
  },
  countContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
});

export default Vista;
