function Ball () {
  this.x = 0
  this.y = 0
  this.size = 50
}

Ball.prototype.setPosition = function(x, y) {
  this.x = x
  this.y = y
}

Ball.prototype.getPosition = function() {
  return { x: this.x, y: this.y}
}