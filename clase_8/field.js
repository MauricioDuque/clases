class Field {
  constructor(canvas){
    this.width = canvas.width || null
    this.height = canvas.height || null
    this.ctx = canvas.getContext('2d') || null
    this.ball = null
    this.posBall = {
      x: 0,
      y: 0
    }
  }

  init() {
    this.ball = new Ball()
    this.ball.size = 25
  }

  exe(){
    this.clear()
    this.drawCmp()
    this.ball.setPosition(this.posBall.x, this.posBall.y++)
    // console.log(this.ball.getPosition())
  }
}


// class Field (canvas) {
//   constructor () {
//     // this.width = canvas.width
//     // this.height = canvas.height
//     // this.ctx = canvas.getContext('2d')
//     // this.ball = null
//     // this.posBall = {
//     //   x: 0,
//     //   y: 0
//     // }
//   }
// }




// function Field (canvas) {
//   this.width = canvas.width
//   this.height = canvas.height
//   this.ctx = canvas.getContext('2d')
//   this.ball = null
//   this.posBall = {
//     x: 0,
//     y: 0
//   }
// }

// Field.prototype.init = function(){
//   this.ball = new Ball()
//   // this.ball.size = 25
// }

// Field.prototype.exe = function(){
//   this.clear()
//   this.drawCmp()
//   this.ball.setPosition(this.posBall.x, this.posBall.y++)
//   // console.log(this.ball.getPosition())
// }

// Field.prototype.clear = function(){
//   this.ctx.clearRect(0,0, this.width, this.height)
// }

// Field.prototype.drawCmp = function(){
//   this.ctx.fillRect(
//     this.ball.getPosition().x,
//     this.ball.getPosition().y,
//     this.ball.size,
//     this.ball.size
//   )
// }