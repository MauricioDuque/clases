var canvas = null
var ctx = null
var counterX = 0
var counterY = 0
var controlCountX = true
var controlCountY = true
var canvasWidth = 600
var canvasHeight = 600


var conCountXBefore = null
var players = {
  one: 0,
  two: 0,
}

window.onload = () => {
  canvas = document.getElementById("myCanvas")
  ctx = canvas.getContext("2d")
}

let exec = () => {
  controlCounter()
  clearAll()
  square(counterX + 100, counterY - 10)
  world(counterX + 100, counterY - 10)
}


let clearAll = () => {
  ctx.clearRect(0,0, 600, 600)
}

let square = (x, y) => {
  this.x = x
  this.y = y
  ctx.fillRect(this.x, this.y, 50, 50)
}

let world = (x, y) => {
  if(x + 50 >= canvasWidth) {
    controlCountX = false
    score()
  }
  if(y + 50 >= canvasHeight) controlCountY = false
  if(x <= 0) {
    controlCountX = true
    score()
  }
  if(y <= 0) controlCountY = true
}

let controlCounter = () => {
  if(controlCountX) counterX++
  else counterX--
  if(controlCountY) counterY++
  else counterY--
}

let score = () => {
  if(conCountXBefore) {
    players.two++
    conCountXBefore = false
    console.log(players)
  }

  if(!conCountXBefore) {
    players.one++
    conCountXBefore = true
  }

  // if(controlCountX) players.two++
  // else players.one++
}

 
setInterval(() => {
  exec()
}, 10);